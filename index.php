<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Xgame</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/smoothbox.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" >
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>
<body>

<div class="content">

    <div class="xgame" >
        <img src="images/xgame.png" data-aos="zoom-in-right"  data-aos-easing="linear"
             data-aos-duration="1000" alt="">
        <p data-aos="zoom-in-left" data-aos-easing="linear"
           data-aos-duration="1000">GAME <br> <span>Компьютерный клуб</span></p>
    </div>
    <div class="light">
        <img src="images/light.png" data-aos="zoom-in-right" data-aos-easing="linear"
             data-aos-duration="1000">
    </div>
    <div class="down-btn" >
        <a href="#services"><img src="images/arrow-down.png"  data-aos="zoom-in-up"  data-aos-delay="300" alt=""></a>
    </div>
    <header class="header" id="main">
        <div class="container-fluid">
            <div class="flex">
                <div class="main-left">
                    <div class="row">
                        <div class="col-sm-6 col-6">
                            <div class="logo" data-aos="fade-down" >
                                <a  href=""><img src="images/logo.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-6">
                            <div class="callback" data-aos="fade-in">
                                <a href="">
                                    <img src="images/phone-ico.png" alt="">
                                    <p>ЗАКАЗАТЬ ЗВОНОК</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="main-nav" data-aos="fade-right" data-aos-delay="300">
                        <ul>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview white"><a href='#main' class="active"><p>Главная</p><span> 01</span></a></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview white"><a href='#services'><p>Услуги</p><span>02</span></a></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview white"><a href='#about-us'><p>О нас</p><span>03</span></a></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview white"><a href='#news'><p>Новости</p><span>04</span></a></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview"></li>
                            <li class="css-shapes-preview white"><a href='#contacts'><p>Контакты</p><span>05</span></a></li>
                            <li class="css-shapes-preview"></li>
                        </ul>
                    </div>
<!--                    mobile-menu-->
                    <div class="menu-icon">
                        <content class="mob-view" >
                            <input id="hamburger" class="hamburger" type="checkbox">
                            <div class="menu-text">
                                <p>menu</p>
                            </div>
                            <label class="hamburger" for="hamburger">
                                <i></i>
                                <text style="display:none;">
                                    <close>закрыть</close>
                                    <open>меню</open>
                                </text>
                            </label>
                            <section class="drawer-list">
                                <ul>
                                    <li><a href="tel:+7 (727) 317-17-88">+7 (727) 317-17-88</a></li>
                                    <li>
                                        <a data-id="1" href="#main">Главная</a>
                                    </li>
                                    <li>
                                        <a data-id="2" href="#services">Услуги
                                        </a>
                                    </li>
                                    <li>
                                        <a data-id="3" href="#about-us">О нас</a>
                                    </li>
                                    <li>
                                        <a data-id="4" href="#news">Новости</a>
                                    </li>
                                    <li>
                                        <a data-id="5" href="#contacts">Контакты</a>
                                    </li>
                                </ul>
                            </section>
                        </content>
                    </div>
                </div>
                <div class="main-right">
                    <div class="main-owl owl-carousel owl-theme" >
                        <div class="item">
                            <img src="images/slide2.jpg"  class="img-fluid" alt="">
                        </div>
                        <div class="item">
                            <img src="images/slide1.jpg"  class="img-fluid" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </header>
<!--    magnific popup-->
    <div class="services" id="services">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-2 col-md-12 col-lg-9">
                    <div class="main-title">
                        <h3>УСЛУГИ</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <div class="service-item" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000" data-aos-easing="ease-in-out">
                                <div class="top">
                                    <h6>ОБЩИЙ ЗАЛ</h6>
                                </div>
                                <div class="bot">
                                    <div class="image">
                                        <img src="images/img1.png" class="img-fluid" alt="">
                                        <div class="count">40</div>
                                    </div>
                                    <p>игровых компьютеров
                                        самой удобной посадки</p>
                                    <div class="red-btn">
                                        <a href="">смотреть зал</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <div class="service-item" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000" data-aos-easing="ease-in-out">
                                <div class="top">
                                    <h6>VIP МЕСТА</h6>
                                </div>
                                <div class="bot">
                                    <div class="image">
                                        <img src="images/img2.png" class="img-fluid" alt="">
                                        <div class="count">18</div>
                                    </div>
                                    <p>игровых компьютеров
                                        самой удобной посадки</p>
                                    <div class="blue-btn">
                                        <a href="">смотреть зал</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <div class="service-item" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000" data-aos-easing="ease-in-out">
                                <div class="top">
                                    <h6>PLAYSTATION 4</h6>
                                </div>
                                <div class="bot">
                                    <div class="image">
                                        <img src="images/img3.png" class="img-fluid" alt="">
                                        <div class="count">18</div>
                                    </div>
                                    <p>игровых компьютеров
                                        самой удобной посадки</p>
                                    <div class="yellow-btn">
                                        <a href="">смотреть зал</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-title">
                        <h3>АКЦИИ</h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <div class="promo-item" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000" data-aos-easing="ease-in-out">
                                <div class="price">
                                    <p><img src="images/blue-star.png" alt="">1500 + 1000</p>
                                </div>
                                <div class="description">
                                    <div class="bar">
                                        <img src="images/bar1.png" alt="">
                                    </div>
                                    <p>Пополни аккаунт на <br>
                                        <b>1500</b> тг и получи <br>
                                        <b>1000</b> тг в подарок!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <div class="promo-item" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000" data-aos-easing="ease-in-out">
                                <div class="price">
                                    <p><img src="images/yellow.png" alt="">3000 + 2500</p>
                                </div>
                                <div class="description">
                                    <div class="bar">
                                        <img src="images/bar2.png" alt="">
                                    </div>
                                    <p>Пополни аккаунт на <br>
                                        <b>3000</b> тг и получи <br>
                                        <b>2500</b> тг в подарок!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-12 col-lg-4">
                            <div class="promo-item" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000" data-aos-easing="ease-in-out">
                                <div class="price">
                                    <p><img src="images/blue-star.png" alt="">5000 + 5000</p>
                                </div>
                                <div class="description">
                                    <div class="bar">
                                        <img src="images/bar3.png" alt="">
                                    </div>
                                    <p>Пополни аккаунт на
                                        <b>5000</b> тг и получи
                                        ту же сумму в подарок!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="left-img">
                    <img src="images/bg-img2.png" class="img-fluid" alt="">
                </div>
                <div class="right-img">
                    <img src="images/bg-img1.png" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="about-us" id="about-us">
        <div class="container-fluid">
            <div class="row">
               <div class="offset-lg-2 col-md-12 col-lg-9">
                   <div class="main-title about-title">
                       <h3>О НАС</h3>
                   </div>
                    <div class="row">
                        <div class="col-sm-5" data-aos="fade-right" data-aos-delay="500" data-aos-duration="1000" data-aos-easing="ease-in-out">
                            <div class="about-subtitle">
                                <div class="image">
                                    <img src="images/about-ico.png" alt="">
                                </div>
                                <p>Рады приветствовать Вас в интернет
                                    клубе X-Game! Готовы предложить Вам:</p>
                            </div>
                            <div class="about-list">
                                <ul>
                                    <li>Высокоскоростной интернет</li>
                                    <li>Отличный сервис</li>
                                    <li>Приятную и уютную обстановку</li>
                                    <li>Компьютеры последней комплектации</li>
                                    <li>Низкие цены</li>
                                    <li>Все последние обновления игр, патчей</li>
                                    <li>Вкусные закуски, холодные напитки, горячий кофе</li>
                                    <li>Максимально все условия для геймеров</li>
                                </ul>
                            </div>
                            <div class="socials">
                                <a href=""><i class="fab fa-facebook-f"></i></a>
                                <a href=""><i class="fab fa-instagram"></i></a>
                                <a href=""><i class="fab fa-vk"></i></a>
                            </div>
                        </div>
                        <div class="col-sm-7" data-aos="fade-left" data-aos-delay="500" data-aos-duration="1000" data-aos-easing="ease-in-out">
                            <div class="owl1 owl-carousel owl-theme">
                                <div class="item">
                                    <img src="images/slide1.jpg"  class="img-fluid" alt="">
                                    <div class="overlay">
                                        <p>ОБЩИЙ ЗАЛ</p>
                                        <div class="zoom-ico parent-container">
                                            <a  href="images/slide1.jpg"><img src="images/zoom-ico.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <img src="images/slide2.jpg"  class="img-fluid" alt="">
                                    <div class="overlay">
                                        <p>ОБЩИЙ ЗАЛ2</p>
                                        <div class="zoom-ico parent-container">
                                            <a  href="images/slide2.jpg"><img src="images/zoom-ico.png" alt=""></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
    <div class="news" id="news">
        <div class="container-fluid">
            <div class="row">
                <div class="offset-lg-2 col-md-12 col-lg-9">

                    <div class="rel-bg">
                        <div class="main-title ">
                            <h3>НОВОСТИ</h3>
                        </div>
                        <div class="calendar" data-aos="zoom-in"  data-aos-easing="ease-in-out">
                            <a href=""><img src="images/calendar.png" alt=""></a>
                        </div>
                       <div class="owl-wrap">
                           <div class="angel" >
                               <img src="images/angel.png" alt="">
                           </div>
                           <div class="owl2 owl-carousel owl-theme">
                               <div class="item">
                                   <div class="news-item" data-aos="zoom-in-up">
                                       <a href="">
                                           <div class="text-block">
                                               <div class="date">
                                                   <p>16.08.18</p>
                                               </div>
                                               <div class="title">
                                                   <h5>17.08.18 Состоится открытие новой точки xGame</h5>
                                               </div>
                                               <div class="text">
                                                   <p>Дорогие друзья xGame, спешим сообщить вам радостную новость — уже завтра состоится открытие новой точки xGame Sputnik! Каждый (!) клиент получит подарочный сертификат на время, энергетик Gorilla и море позитивного настроения Ждем вас всех завтра в гости в 17:00! Мамыр 1, д. 8А</p>
                                               </div>
                                           </div>
                                           <div class="image" data-aos="fade-left">
                                               <img src="images/news-img.jpg"  alt="">
                                           </div>
                                       </a>
                                   </div>
                                   <div class="separate"></div>
                                   <div class="news-item" data-aos="zoom-in-up">
                                       <a href="">
                                           <div class="text-block">
                                               <div class="date">
                                                   <p>16.08.18</p>
                                               </div>
                                               <div class="title">
                                                   <h5>17.08.18 Состоится открытие новой точки xGame</h5>
                                               </div>
                                               <div class="text">
                                                   <p>Дорогие друзья xGame, спешим сообщить вам радостную новость — уже завтра состоится открытие новой точки xGame Sputnik! Каждый (!) клиент получит подарочный сертификат на время, энергетик Gorilla и море позитивного настроения Ждем вас всех завтра в гости в 17:00! Мамыр 1, д. 8А</p>
                                               </div>
                                           </div>
                                           <div class="image">
                                               <img src="images/news-img.jpg"  alt="">
                                           </div>
                                       </a>
                                   </div>
                               </div>
                               <div class="item">
                                   <div class="news-item" data-aos="zoom-in-up">
                                       <a href="">
                                           <div class="text-block">
                                               <div class="date">
                                                   <p>16.08.18</p>
                                               </div>
                                               <div class="title">
                                                   <h5>17.08.18 Состоится открытие новой точки xGame</h5>
                                               </div>
                                               <div class="text">
                                                   <p>Дорогие друзья xGame, спешим сообщить вам радостную новость — уже завтра состоится открытие новой точки xGame Sputnik! Каждый (!) клиент получит подарочный сертификат на время, энергетик Gorilla и море позитивного настроения Ждем вас всех завтра в гости в 17:00! Мамыр 1, д. 8А</p>
                                               </div>
                                           </div>
                                           <div class="image">
                                               <img src="images/news-img.jpg"  alt="">
                                           </div>
                                       </a>
                                   </div>
                                   <div class="separate"></div>
                                   <div class="news-item" data-aos="zoom-in-up">
                                       <a href="">
                                           <div class="text-block">
                                               <div class="date">
                                                   <p>16.08.18</p>
                                               </div>
                                               <div class="title">
                                                   <h5>17.08.18 Состоится открытие новой точки xGame</h5>
                                               </div>
                                               <div class="text">
                                                   <p>Дорогие друзья xGame, спешим сообщить вам радостную новость — уже завтра состоится открытие новой точки xGame Sputnik! Каждый (!) клиент получит подарочный сертификат на время, энергетик Gorilla и море позитивного настроения Ждем вас всех завтра в гости в 17:00! Мамыр 1, д. 8А</p>
                                               </div>
                                           </div>
                                           <div class="image">
                                               <img src="images/news-img.jpg"  alt="">
                                           </div>
                                       </a>
                                   </div>
                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts" id="contacts">
        <div class="overlay">
            <div class="gallery-modal">
                <div class="close-modal">
                    <a href="">&#10005</a>
                </div>
                <div class="bot-album">
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                    <a href="images/gallery.png"><img src="images/gallery.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="offset-lg-2 col-md-12 col-lg-9">
                <div class="main-title ">
                    <h3>ФОТОГАЛЕРЕЯ</h3>
                </div>
                <div class="gallery" data-aos="zoom-out">
                    <div class="album-name">
                        <h6>ВСЕ АЛЬБОМЫ</h6>
                    </div>
                    <div class="owl3 owl-carousel owl-theme">
                        <div class="item">
                            <div class="open-modal">
                                <img src="images/gallery-arrow.png" alt="">
                            </div>
                            <img src="images/gallery.png" class="img-fluid" alt="">
                            <div class="overlay">
                                <p>INTERNATIONAL 2018</p>
                                <div class="zoom-ico">
                                    <a  href="#"><img src="images/zoom-ico.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="open-modal">
                                <img src="images/gallery-arrow.png" alt="">
                            </div>
                            <img src="images/gallery.png" class="img-fluid" alt="">
                            <div class="overlay">
                                <p>INTERNATIONAL 2018</p>
                                <div class="zoom-ico">
                                    <a  href="#"><img src="images/zoom-ico.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="open-modal">
                                <img src="images/gallery-arrow.png" alt="">
                            </div>
                            <img src="images/gallery.png" class="img-fluid" alt="">
                            <div class="overlay">
                                <p>INTERNATIONAL 2018</p>
                                <div class="zoom-ico">
                                    <a  href="#"><img src="images/zoom-ico.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-title ">
                    <h3>КОНТАКТЫ</h3>
                </div>
                <div class="row">
                    <div class="col-sm-4" data-aos="fade-up" data-aos-delay="300" data-aos-duration="1000" data-aos-easing="ease-in-out">
                        <div class="contact-item">
                            <h6>XGAME club new</h6>
                            <p>Алматы, PlayStation Center
                                Globus, Алматы, Абая проспект, 109в
                            </p>
                            <a href="tel:+7 (727) 356 58 68">+7 (727) 356 58 68</a>
                            <a href="mailto:smmxgameclub@gmail.com">smmxgameclub@gmail.com</a>
                        </div>
                    </div>
                    <div class="col-sm-4" data-aos="fade-up" data-aos-delay="400" data-aos-duration="1000" data-aos-easing="ease-in-out">
                        <div class="contact-item">
                            <h6>XGAME club new</h6>
                            <p>Алматы, PlayStation Center
                                Globus, Алматы, Абая проспект, 109в
                            </p>
                            <a href="tel:+7 (727) 356 58 68">+7 (727) 356 58 68</a>
                            <a href="mailto:smmxgameclub@gmail.com">smmxgameclub@gmail.com</a>
                        </div>
                    </div>
                    <div class="col-sm-4" data-aos="fade-up" data-aos-delay="500" data-aos-duration="1000" data-aos-easing="ease-in-out">
                        <div class="contact-item">
                            <h6>XGAME club new</h6>
                            <p>Алматы, PlayStation Center
                                Globus, Алматы, Абая проспект, 109в
                            </p>
                            <a href="tel:+7 (727) 356 58 68">+7 (727) 356 58 68</a>
                            <a href="mailto:smmxgameclub@gmail.com">smmxgameclub@gmail.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
<script src="js/jquery-3.2.1.min.js"></script>


<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.js"></script>
<script src="js/main.js"></script>


</html>

