(function($) {
    $('.open-modal').click(function (e) {
        e.preventDefault();
        $('.gallery-modal, .overlay').show();
    })
    $('.close-modal').click(function (e) {
        e.preventDefault();
        $('.gallery-modal, .overlay').hide();
    })

    AOS.init();
    $('.down-btn').hover(function () {
        $('.down-btn img').toggleClass('wow animated tada');
    })
    $('.main-owl').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        autoplay:true,
        autoplayTimeout:5000,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1,
                loop:true
            }
        }
    })
    $('.owl1').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:true
            },
            1000:{
                items:1,
                nav:true,
                loop:true
            }
        }
    })
    $('.owl2').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:true

            },
            1000:{
                items:1,
                nav:true,
                loop:true
            }
        }
    })
    $('.owl3').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        dots:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:true
            },
            1000:{
                items:1,
                nav:true,
                loop:true
            }
        }
    })

    new WOW().init();
})(jQuery);

$(document).ready(function () {
    $('.parent-container').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
    });
    $('.bot-album').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true
        },
    });

    $(document).on("scroll", onScroll);

    //smoothscroll
    $('a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');

        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top+2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });
});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.main-nav ul li a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
            $('.main-nav ul li a').removeClass("active");
            currLink.addClass("active");
        }
        // else{
        //     currLink.removeClass("active");
        // }
    });
}





